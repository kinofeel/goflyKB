#### 批量插入
```
    <!-- 批量添加资金流水 -->
    <insert id="batchInsertFlowData" parameterType="java.util.List">
        insert into AC_FUND_FLOW_DETAIL (ID, AID, FLOW_CODE, AMOUNT, DEBIT, CREATE_DATE)
        <foreach collection="list" item="item" index="index" separator="union all">
            (select GET_SEQUENCE(22), #{item.aid}, #{item.flowCode}, #{item.amount}, #{item.debit}, sysdate from dual)
        </foreach>
    </insert>
```


#### 批量更新
```
    <!-- 批量更新资金记录的最新余额 -->
    <update id="batchUpdateFlowBalance" parameterType="java.util.List">
        UPDATE AC_FUND_FLOW_DETAIL
        <trim prefix="set" suffixOverrides=",">
            <trim prefix=" balance = case" suffix="end,">
                <foreach collection="list" item="i" index="index">
                    when id = #{i.id, jdbcType = BIGINT} then 
                    #{i.balance, jdbcType = DECIMAL}
                </foreach>
            </trim>
        </trim>
        WHERE
        <foreach collection="list" separator="or" item="i" index="index">
            id = #{i.id, jdbcType = BIGINT}
        </foreach>
    </update>
```

#### 大于等于>=及小于等于 转义
```
<select id="searchByPrice" parameterType="Map" resultType="Product">
    <!-- 方式1、转义字符 -->
    select * from Product where price &gt;= #{minPrice} and price &lt;= #{maxPrice}
</select>

<select id="searchByPrice" parameterType="Map" resultType="Product">
  <!-- 方式2、CDATA -->
  <![CDATA[select * from Product where price >= #{minPrice} and price <= #{maxPrice} ]]>
</select>

转义	符号
&lt;	<
&gt;	>
&amp;	&
&apos;	’
&quot;	“
```


#### Mybatis-Plus乐观锁插件使用
```
第一步：
@Bean
public OptimisticLockerInterceptor optimisticLockerInterceptor() {
    return new OptimisticLockerInterceptor();
}
第二步：
@Version
private Integer version;
```

[ :clap: Mybatis-Plus官方乐观锁完整使用介绍](https://mp.baomidou.com/guide/optimistic-locker-plugin.html#%E4%B8%BB%E8%A6%81%E9%80%82%E7%94%A8%E5%9C%BA%E6%99%AF)