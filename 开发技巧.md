#### IDEA插件-restfultoolkit
> 方便测试rest接口，不可以自定义端口

[插件restfultoolkit官网](https://plugins.jetbrains.com/plugin/10292-restfultoolkit)


#### logbak手册
[手册信息](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-logging-console-output)


#### java字符串格式手册
[格式手册](https://blog.csdn.net/lonely_fireworks/article/details/7962171)

#### jRebel激活方法
> 连接输入url输入ip后再加个生成GUID的内容 dxl@163.com
[操作方法](https://www.cnblogs.com/dxllp/p/11217546.html)


#### Java中存储金额用什么数据类型
> 四则运算


![输入图片说明](https://images.gitee.com/uploads/images/2019/1029/111204_f6baf809_1636555.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/1029/111252_ea328509_1636555.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/1029/111311_16d5c2ba_1636555.png "屏幕截图.png")

[ :clap: 点击我进入详细介绍](https://my.oschina.net/u/3162080/blog/1595706)

#### 时间格式化
@DatetimeFormat是将String转换成Date，一般前台给后台传值时用
```
@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
private Date sendTime;
```

@JsonFormat将Date转换成String  一般后台传值给前台时

```
@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
private Date sendTime;
```